const express = require("express");
const dotenv = require("dotenv");
const morgan = require('morgan');
const colors = require('colors');
const todoRoutes = require("./routes/todo.routes");
const mongodb = require("./config/db");

const app = express();

dotenv.config({ path: './config/config.env' });

mongodb.connectDB();

app.use(express.json());

app.use("/todos", todoRoutes);

app.use((error, req, res, next) => {
  res.status(500).json({ message: error.message });
});

app.get("/", (req, res) => {
  res.json("hello world");
});

module.exports = app;
